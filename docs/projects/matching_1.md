# Matching - Example 1

Dataset used - [Right Heart Catheterization Dataset]('http://biostat.mc.vanderbilt.edu/wiki/pub/Main/DataSets/rhc.csv')


## Introduction

In this notebook, we are going to estimate the causal effect of Right Heart Catheterization technique on the deaths of the patients after the heart surgeries. 


```
# !pip install dowhy
# !pip install causallib
```


```python
import pandas as pd
import numpy as np
from dowhy import CausalModel
```

## Fetch data


```python
df = pd.read_csv('http://biostat.mc.vanderbilt.edu/wiki/pub/Main/DataSets/rhc.csv')
```


```python
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
    
    table{
       overflow-y:scroll;
       height:30%;
       display:block;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Unnamed: 0</th>
      <th>cat1</th>
      <th>cat2</th>
      <th>ca</th>
      <th>sadmdte</th>
      <th>dschdte</th>
      <th>dthdte</th>
      <th>lstctdte</th>
      <th>death</th>
      <th>cardiohx</th>
      <th>chfhx</th>
      <th>dementhx</th>
      <th>psychhx</th>
      <th>chrpulhx</th>
      <th>renalhx</th>
      <th>liverhx</th>
      <th>gibledhx</th>
      <th>malighx</th>
      <th>immunhx</th>
      <th>transhx</th>
      <th>amihx</th>
      <th>age</th>
      <th>sex</th>
      <th>edu</th>
      <th>surv2md1</th>
      <th>das2d3pc</th>
      <th>t3d30</th>
      <th>dth30</th>
      <th>aps1</th>
      <th>scoma1</th>
      <th>meanbp1</th>
      <th>wblc1</th>
      <th>hrt1</th>
      <th>resp1</th>
      <th>temp1</th>
      <th>pafi1</th>
      <th>alb1</th>
      <th>hema1</th>
      <th>bili1</th>
      <th>crea1</th>
      <th>sod1</th>
      <th>pot1</th>
      <th>paco21</th>
      <th>ph1</th>
      <th>swang1</th>
      <th>wtkilo1</th>
      <th>dnr1</th>
      <th>ninsclas</th>
      <th>resp</th>
      <th>card</th>
      <th>neuro</th>
      <th>gastr</th>
      <th>renal</th>
      <th>meta</th>
      <th>hema</th>
      <th>seps</th>
      <th>trauma</th>
      <th>ortho</th>
      <th>adld3p</th>
      <th>urin1</th>
      <th>race</th>
      <th>income</th>
      <th>ptid</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>COPD</td>
      <td>NaN</td>
      <td>Yes</td>
      <td>11142</td>
      <td>11151.0</td>
      <td>NaN</td>
      <td>11382</td>
      <td>No</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>70.25098</td>
      <td>Male</td>
      <td>12.000000</td>
      <td>0.640991</td>
      <td>23.50000</td>
      <td>30</td>
      <td>No</td>
      <td>46</td>
      <td>0</td>
      <td>41.0</td>
      <td>22.097656</td>
      <td>124</td>
      <td>10.0</td>
      <td>38.69531</td>
      <td>68.00000</td>
      <td>3.500000</td>
      <td>58.000000</td>
      <td>1.009766</td>
      <td>1.199951</td>
      <td>145</td>
      <td>4.000000</td>
      <td>40.0</td>
      <td>7.359375</td>
      <td>No RHC</td>
      <td>64.69995</td>
      <td>No</td>
      <td>Medicare</td>
      <td>Yes</td>
      <td>Yes</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>0.0</td>
      <td>NaN</td>
      <td>white</td>
      <td>Under $11k</td>
      <td>5</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>MOSF w/Sepsis</td>
      <td>NaN</td>
      <td>No</td>
      <td>11799</td>
      <td>11844.0</td>
      <td>11844.0</td>
      <td>11844</td>
      <td>Yes</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>78.17896</td>
      <td>Female</td>
      <td>12.000000</td>
      <td>0.755000</td>
      <td>14.75195</td>
      <td>30</td>
      <td>No</td>
      <td>50</td>
      <td>0</td>
      <td>63.0</td>
      <td>28.898438</td>
      <td>137</td>
      <td>38.0</td>
      <td>38.89844</td>
      <td>218.31250</td>
      <td>2.599609</td>
      <td>32.500000</td>
      <td>0.699951</td>
      <td>0.599976</td>
      <td>137</td>
      <td>3.299805</td>
      <td>34.0</td>
      <td>7.329102</td>
      <td>RHC</td>
      <td>45.69998</td>
      <td>No</td>
      <td>Private &amp; Medicare</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>Yes</td>
      <td>No</td>
      <td>No</td>
      <td>NaN</td>
      <td>1437.0</td>
      <td>white</td>
      <td>Under $11k</td>
      <td>7</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>MOSF w/Malignancy</td>
      <td>MOSF w/Sepsis</td>
      <td>Yes</td>
      <td>12083</td>
      <td>12143.0</td>
      <td>NaN</td>
      <td>12400</td>
      <td>No</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>46.09198</td>
      <td>Female</td>
      <td>14.069916</td>
      <td>0.317000</td>
      <td>18.13672</td>
      <td>30</td>
      <td>No</td>
      <td>82</td>
      <td>0</td>
      <td>57.0</td>
      <td>0.049995</td>
      <td>130</td>
      <td>40.0</td>
      <td>36.39844</td>
      <td>275.50000</td>
      <td>3.500000</td>
      <td>21.097656</td>
      <td>1.009766</td>
      <td>2.599609</td>
      <td>146</td>
      <td>2.899902</td>
      <td>16.0</td>
      <td>7.359375</td>
      <td>RHC</td>
      <td>0.00000</td>
      <td>No</td>
      <td>Private</td>
      <td>No</td>
      <td>Yes</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>NaN</td>
      <td>599.0</td>
      <td>white</td>
      <td>$25-$50k</td>
      <td>9</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>ARF</td>
      <td>NaN</td>
      <td>No</td>
      <td>11146</td>
      <td>11183.0</td>
      <td>11183.0</td>
      <td>11182</td>
      <td>Yes</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>75.33197</td>
      <td>Female</td>
      <td>9.000000</td>
      <td>0.440979</td>
      <td>22.92969</td>
      <td>30</td>
      <td>No</td>
      <td>48</td>
      <td>0</td>
      <td>55.0</td>
      <td>23.296875</td>
      <td>58</td>
      <td>26.0</td>
      <td>35.79688</td>
      <td>156.65625</td>
      <td>3.500000</td>
      <td>26.296875</td>
      <td>0.399963</td>
      <td>1.699951</td>
      <td>117</td>
      <td>5.799805</td>
      <td>30.0</td>
      <td>7.459961</td>
      <td>No RHC</td>
      <td>54.59998</td>
      <td>No</td>
      <td>Private &amp; Medicare</td>
      <td>Yes</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>white</td>
      <td>$11-$25k</td>
      <td>10</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>MOSF w/Sepsis</td>
      <td>NaN</td>
      <td>No</td>
      <td>12035</td>
      <td>12037.0</td>
      <td>12037.0</td>
      <td>12036</td>
      <td>Yes</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>67.90997</td>
      <td>Male</td>
      <td>9.945259</td>
      <td>0.437000</td>
      <td>21.05078</td>
      <td>2</td>
      <td>Yes</td>
      <td>72</td>
      <td>41</td>
      <td>65.0</td>
      <td>29.699219</td>
      <td>125</td>
      <td>27.0</td>
      <td>34.79688</td>
      <td>478.00000</td>
      <td>3.500000</td>
      <td>24.000000</td>
      <td>1.009766</td>
      <td>3.599609</td>
      <td>126</td>
      <td>5.799805</td>
      <td>17.0</td>
      <td>7.229492</td>
      <td>RHC</td>
      <td>78.39996</td>
      <td>Yes</td>
      <td>Medicare</td>
      <td>No</td>
      <td>Yes</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>No</td>
      <td>NaN</td>
      <td>64.0</td>
      <td>white</td>
      <td>Under $11k</td>
      <td>11</td>
    </tr>
  </tbody>
</table>
</div>



## Knowing the data


```python
df.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 5735 entries, 0 to 5734
    Data columns (total 63 columns):
     #   Column      Non-Null Count  Dtype  
    ---  ------      --------------  -----  
     0   Unnamed: 0  5735 non-null   int64  
     1   cat1        5735 non-null   object 
     2   cat2        1200 non-null   object 
     3   ca          5735 non-null   object 
     4   sadmdte     5735 non-null   int64  
     5   dschdte     5734 non-null   float64
     6   dthdte      3722 non-null   float64
     7   lstctdte    5735 non-null   int64  
     8   death       5735 non-null   object 
     9   cardiohx    5735 non-null   int64  
     10  chfhx       5735 non-null   int64  
     11  dementhx    5735 non-null   int64  
     12  psychhx     5735 non-null   int64  
     13  chrpulhx    5735 non-null   int64  
     14  renalhx     5735 non-null   int64  
     15  liverhx     5735 non-null   int64  
     16  gibledhx    5735 non-null   int64  
     17  malighx     5735 non-null   int64  
     18  immunhx     5735 non-null   int64  
     19  transhx     5735 non-null   int64  
     20  amihx       5735 non-null   int64  
     21  age         5735 non-null   float64
     22  sex         5735 non-null   object 
     23  edu         5735 non-null   float64
     24  surv2md1    5735 non-null   float64
     25  das2d3pc    5735 non-null   float64
     26  t3d30       5735 non-null   int64  
     27  dth30       5735 non-null   object 
     28  aps1        5735 non-null   int64  
     29  scoma1      5735 non-null   int64  
     30  meanbp1     5735 non-null   float64
     31  wblc1       5735 non-null   float64
     32  hrt1        5735 non-null   int64  
     33  resp1       5735 non-null   float64
     34  temp1       5735 non-null   float64
     35  pafi1       5735 non-null   float64
     36  alb1        5735 non-null   float64
     37  hema1       5735 non-null   float64
     38  bili1       5735 non-null   float64
     39  crea1       5735 non-null   float64
     40  sod1        5735 non-null   int64  
     41  pot1        5735 non-null   float64
     42  paco21      5735 non-null   float64
     43  ph1         5735 non-null   float64
     44  swang1      5735 non-null   object 
     45  wtkilo1     5735 non-null   float64
     46  dnr1        5735 non-null   object 
     47  ninsclas    5735 non-null   object 
     48  resp        5735 non-null   object 
     49  card        5735 non-null   object 
     50  neuro       5735 non-null   object 
     51  gastr       5735 non-null   object 
     52  renal       5735 non-null   object 
     53  meta        5735 non-null   object 
     54  hema        5735 non-null   object 
     55  seps        5735 non-null   object 
     56  trauma      5735 non-null   object 
     57  ortho       5735 non-null   object 
     58  adld3p      1439 non-null   float64
     59  urin1       2707 non-null   float64
     60  race        5735 non-null   object 
     61  income      5735 non-null   object 
     62  ptid        5735 non-null   int64  
    dtypes: float64(21), int64(21), object(21)
    memory usage: 2.8+ MB


So there are 5735 rows and 63 columns.  
Description of each and every column for this dataset can be found [here](http://biostat.mc.vanderbilt.edu/wiki/pub/Main/DataSets/rhc.html).
Not that more than 25% of missing values are found for the columns - cat2, dthdte, adld3p, urin1. For now we can discard these columns and we can find ways to include them if the final results are not satisfactory i.e. if we find the model to be too sensitive to the hidden bias as per the sentivity analysis.  
We will covert columns having values 'Yes' and 'No' into 1s and 0s.  
We also need to convert categorical variable into numerical for analysis. We will one hot encode such variables.  
We will also drop some variables which are not related to the treatment outcome.




## Pre-processing

Before we know the details of the data, it is better to transform the data into the required data. In other words, we will pre-process the data in order to clean it.


```python
# drop not necessary columns
df.drop(['Unnamed: 0', 'ptid', 'dth30', 'cat2', 'urin1', 'dthdte', 'adld3p', 'lstctdte', 'dschdte', 'income', 'edu', 'das2d3pc', 'dnr1', 'sadmdte', 'ninsclas'], axis=1, inplace=True, errors='ignore')
```


```python
# replace 'Yes' and 'No' with 1s and 0s
result = {'Yes':1, 'No':0}
df[['death', 'ca', 'resp', 'card', 'neuro', 'gastr', 'renal', 'meta', 'hema', 'seps', 'trauma', 'ortho']] = df[['death', 'ca', 'resp', 'card', 'neuro', 'gastr', 'renal', 'meta', 'hema', 'seps', 'trauma', 'ortho']].replace(result)
```


```python
# one hot encode the required categorical values
df = pd.get_dummies(df, columns=['cat1', 'sex', 'race'], drop_first=True)
```


```python
# replace RHC with 1 and No-RHC with 0
treat = {'RHC':1, 'No RHC':0}
df['swang1'].replace(treat, inplace=True)
```

Now we have the the required data in the required format.  
Now we can separate the treatment variable and the outcome variable from this dataframe.  
Out treatment variable is 'swang1' while outcome variable is 'death'.  


```python
# treatment = df['swang1']
# outcome = df['death']
# df.drop(columns=['swang1', 'death'], inplace=True, errors='ignore')
```

## Statistical summary


```python
df.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }

    table{
       overflow-y:scroll;
       height:30%;
       display:block;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>death</th>
      <th>cardiohx</th>
      <th>chfhx</th>
      <th>dementhx</th>
      <th>psychhx</th>
      <th>chrpulhx</th>
      <th>renalhx</th>
      <th>liverhx</th>
      <th>gibledhx</th>
      <th>malighx</th>
      <th>immunhx</th>
      <th>transhx</th>
      <th>amihx</th>
      <th>age</th>
      <th>surv2md1</th>
      <th>t3d30</th>
      <th>aps1</th>
      <th>scoma1</th>
      <th>meanbp1</th>
      <th>wblc1</th>
      <th>hrt1</th>
      <th>resp1</th>
      <th>temp1</th>
      <th>pafi1</th>
      <th>alb1</th>
      <th>hema1</th>
      <th>bili1</th>
      <th>crea1</th>
      <th>sod1</th>
      <th>pot1</th>
      <th>paco21</th>
      <th>ph1</th>
      <th>swang1</th>
      <th>wtkilo1</th>
      <th>resp</th>
      <th>card</th>
      <th>neuro</th>
      <th>gastr</th>
      <th>renal</th>
      <th>meta</th>
      <th>hema</th>
      <th>seps</th>
      <th>trauma</th>
      <th>ortho</th>
      <th>cat1_CHF</th>
      <th>cat1_COPD</th>
      <th>cat1_Cirrhosis</th>
      <th>cat1_Colon Cancer</th>
      <th>cat1_Coma</th>
      <th>cat1_Lung Cancer</th>
      <th>cat1_MOSF w/Malignancy</th>
      <th>cat1_MOSF w/Sepsis</th>
      <th>sex_Male</th>
      <th>race_other</th>
      <th>race_white</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
      <td>5735.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>0.648997</td>
      <td>0.176635</td>
      <td>0.178030</td>
      <td>0.098344</td>
      <td>0.067306</td>
      <td>0.189887</td>
      <td>0.044464</td>
      <td>0.069922</td>
      <td>0.032258</td>
      <td>0.229468</td>
      <td>0.269050</td>
      <td>0.115432</td>
      <td>0.034874</td>
      <td>61.375883</td>
      <td>0.592450</td>
      <td>23.612206</td>
      <td>54.667655</td>
      <td>21.004185</td>
      <td>78.520052</td>
      <td>15.645165</td>
      <td>115.178901</td>
      <td>28.092118</td>
      <td>37.618357</td>
      <td>222.273710</td>
      <td>3.092743</td>
      <td>31.865479</td>
      <td>2.267067</td>
      <td>2.133017</td>
      <td>136.768963</td>
      <td>4.066693</td>
      <td>38.748975</td>
      <td>7.388413</td>
      <td>0.380820</td>
      <td>67.827817</td>
      <td>0.368439</td>
      <td>0.336704</td>
      <td>0.120837</td>
      <td>0.164255</td>
      <td>0.051439</td>
      <td>0.046207</td>
      <td>0.061726</td>
      <td>0.179773</td>
      <td>0.009067</td>
      <td>0.001221</td>
      <td>0.079512</td>
      <td>0.079686</td>
      <td>0.039058</td>
      <td>0.001221</td>
      <td>0.076024</td>
      <td>0.006800</td>
      <td>0.069573</td>
      <td>0.213949</td>
      <td>0.556582</td>
      <td>0.061901</td>
      <td>0.777681</td>
    </tr>
    <tr>
      <th>std</th>
      <td>0.477325</td>
      <td>0.381393</td>
      <td>0.382571</td>
      <td>0.297805</td>
      <td>0.250573</td>
      <td>0.392246</td>
      <td>0.206141</td>
      <td>0.255037</td>
      <td>0.176700</td>
      <td>0.420527</td>
      <td>0.443505</td>
      <td>0.319570</td>
      <td>0.183476</td>
      <td>16.681929</td>
      <td>0.195354</td>
      <td>10.052266</td>
      <td>19.956434</td>
      <td>30.266794</td>
      <td>38.046582</td>
      <td>11.867515</td>
      <td>41.244941</td>
      <td>14.078041</td>
      <td>1.774704</td>
      <td>114.953087</td>
      <td>0.783277</td>
      <td>8.363786</td>
      <td>4.801538</td>
      <td>2.053080</td>
      <td>7.655160</td>
      <td>1.028353</td>
      <td>13.183445</td>
      <td>0.109812</td>
      <td>0.485631</td>
      <td>29.055534</td>
      <td>0.482423</td>
      <td>0.472624</td>
      <td>0.325966</td>
      <td>0.370539</td>
      <td>0.220910</td>
      <td>0.209952</td>
      <td>0.240679</td>
      <td>0.384032</td>
      <td>0.094797</td>
      <td>0.034918</td>
      <td>0.270559</td>
      <td>0.270830</td>
      <td>0.193751</td>
      <td>0.034918</td>
      <td>0.265060</td>
      <td>0.082191</td>
      <td>0.254448</td>
      <td>0.410127</td>
      <td>0.496831</td>
      <td>0.240996</td>
      <td>0.415841</td>
    </tr>
    <tr>
      <th>min</th>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>18.041990</td>
      <td>0.000000</td>
      <td>2.000000</td>
      <td>3.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>27.000000</td>
      <td>11.599610</td>
      <td>0.299988</td>
      <td>2.000000</td>
      <td>0.099991</td>
      <td>0.099991</td>
      <td>101.000000</td>
      <td>1.099854</td>
      <td>1.000000</td>
      <td>6.579102</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>50.149490</td>
      <td>0.470947</td>
      <td>16.000000</td>
      <td>41.000000</td>
      <td>0.000000</td>
      <td>50.000000</td>
      <td>8.398438</td>
      <td>97.000000</td>
      <td>14.000000</td>
      <td>36.093750</td>
      <td>133.312500</td>
      <td>2.599609</td>
      <td>26.097656</td>
      <td>0.799927</td>
      <td>1.000000</td>
      <td>132.000000</td>
      <td>3.399902</td>
      <td>31.000000</td>
      <td>7.339844</td>
      <td>0.000000</td>
      <td>56.299990</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>64.047000</td>
      <td>0.628000</td>
      <td>30.000000</td>
      <td>54.000000</td>
      <td>0.000000</td>
      <td>63.000000</td>
      <td>14.099609</td>
      <td>124.000000</td>
      <td>30.000000</td>
      <td>38.093750</td>
      <td>202.500000</td>
      <td>3.500000</td>
      <td>30.000000</td>
      <td>1.009766</td>
      <td>1.500000</td>
      <td>136.000000</td>
      <td>3.799805</td>
      <td>37.000000</td>
      <td>7.399998</td>
      <td>0.000000</td>
      <td>70.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>73.932465</td>
      <td>0.743000</td>
      <td>30.000000</td>
      <td>67.000000</td>
      <td>41.000000</td>
      <td>115.000000</td>
      <td>20.048828</td>
      <td>141.000000</td>
      <td>38.000000</td>
      <td>39.000000</td>
      <td>316.625000</td>
      <td>3.500000</td>
      <td>36.296875</td>
      <td>1.399902</td>
      <td>2.399902</td>
      <td>142.000000</td>
      <td>4.599609</td>
      <td>42.000000</td>
      <td>7.459961</td>
      <td>1.000000</td>
      <td>83.699950</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>101.847960</td>
      <td>0.962000</td>
      <td>30.000000</td>
      <td>147.000000</td>
      <td>100.000000</td>
      <td>259.000000</td>
      <td>192.000000</td>
      <td>250.000000</td>
      <td>100.000000</td>
      <td>43.000000</td>
      <td>937.500000</td>
      <td>29.000000</td>
      <td>66.187500</td>
      <td>58.195312</td>
      <td>25.097656</td>
      <td>178.000000</td>
      <td>11.898438</td>
      <td>156.000000</td>
      <td>7.769531</td>
      <td>1.000000</td>
      <td>244.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
    </tr>
  </tbody>
</table>
</div>



Almost all the columns with binary outcomes have class imbalace in them.  
And almost all continuous variable doesn't have significant skewness and currently I don't know how skewness affects the analysis. 

## Causal Model

## Estimation of causal effect

Since now that we have the data in the required format we can begin our analysis.  
We are going to use Microsoft's [DoWhy](https://microsoft.github.io/dowhy/dowhy_simple_example.html). It is an open source python paackage for causal inference. 

The first step it involves is creation of Causal Model with the data. It takes inputs as data with treatment and outcome columns, name of the treatment variable column in the data, name of the outcome variable column in the data and the column names which we want to consider for confounding. 


```python
# creation of list of confounding variables
common_cause = list(df.columns)
common_cause.remove('swang1')
common_cause.remove('death')
```


```python
# creation of model
model = CausalModel(
    data=df,
    treatment='swang1',
    outcome='death',
    common_causes = common_cause)
```

    WARNING:dowhy.causal_model:Causal Graph not provided. DoWhy will construct a graph based on data inputs.
    INFO:dowhy.causal_graph:If this is observed data (not from a randomized experiment), there might always be missing confounders. Adding a node named "Unobserved Confounders" to reflect this.
    INFO:dowhy.causal_model:Model to find the causal effect of treatment ['swang1'] on outcome ['death']


The next step is to identification of the expression to be computed. For now we have not provided any instrumental variables to the above model. So library currently supports only backdoor path criterion to contruct the expression for computation which works for us too. The model that we created in the above has basically created the Directed Acyclic Graph (DAG) which will be used in the next step of identification.


```python
# identification of estimand
identified_estimand = model.identify_effect(proceed_when_unidentifiable=True)
print(identified_estimand)
```

    INFO:dowhy.causal_identifier:Common causes of treatment and outcome:['immunhx', 'amihx', 'liverhx', 'hema1', 'renal', 'cat1_Lung Cancer', 'meta', 'ortho', 'paco21', 'pafi1', 'psychhx', 'resp', 'malighx', 'wblc1', 'sex_Male', 'temp1', 'crea1', 'gastr', 'chrpulhx', 'ca', 'gibledhx', 'age', 'meanbp1', 'scoma1', 'neuro', 'cat1_Colon Cancer', 'alb1', 'resp1', 'bili1', 'hrt1', 'aps1', 'dementhx', 'transhx', 'cat1_CHF', 'cat1_MOSF w/Malignancy', 'card', 'chfhx', 'ph1', 'surv2md1', 'sod1', 'cardiohx', 'renalhx', 'cat1_MOSF w/Sepsis', 'seps', 'cat1_Cirrhosis', 'trauma', 'race_white', 'cat1_Coma', 'pot1', 'hema', 'U', 'cat1_COPD', 't3d30', 'wtkilo1', 'race_other']
    WARNING:dowhy.causal_identifier:If this is observed data (not from a randomized experiment), there might always be missing confounders. Causal effect cannot be identified perfectly.
    INFO:dowhy.causal_identifier:Continuing by ignoring these unobserved confounders because proceed_when_unidentifiable flag is True.
    INFO:dowhy.causal_identifier:Instrumental variables for treatment and outcome:[]


    Estimand type: nonparametric-ate
    ### Estimand : 1
    Estimand name: backdoor
    Estimand expression:
        d                                                                         
    ─────────(Expectation(death|immunhx,amihx,liverhx,hema1,renal,cat1_Lung Cancer
    d[swang<sub>1</sub>]                                                                     
    
                                                                                  
    ,meta,ortho,paco21,pafi1,psychhx,resp,malighx,wblc1,sex_Male,temp1,crea1,gastr
                                                                                  
    
                                                                                  
    ,chrpulhx,ca,gibledhx,age,meanbp1,scoma1,neuro,cat1_Colon Cancer,alb1,resp1,bi
                                                                                  
    
                                                                                  
    li1,hrt1,aps1,dementhx,transhx,cat1_CHF,cat1_MOSF w/Malignancy,card,chfhx,ph1,
                                                                                  
    
                                                                                  
    surv2md1,sod1,cardiohx,renalhx,cat1_MOSF w/Sepsis,seps,cat1_Cirrhosis,trauma,r
                                                                                  
    
                                                                      
    ace_white,cat1_Coma,pot1,hema,cat1_COPD,t3d30,wtkilo1,race_other))
                                                                      
    Estimand assumption 1, Unconfoundedness: If U→{swang1} and U→death then P(death|swang1,immunhx,amihx,liverhx,hema1,renal,cat1_Lung Cancer,meta,ortho,paco21,pafi1,psychhx,resp,malighx,wblc1,sex_Male,temp1,crea1,gastr,chrpulhx,ca,gibledhx,age,meanbp1,scoma1,neuro,cat1_Colon Cancer,alb1,resp1,bili1,hrt1,aps1,dementhx,transhx,cat1_CHF,cat1_MOSF w/Malignancy,card,chfhx,ph1,surv2md1,sod1,cardiohx,renalhx,cat1_MOSF w/Sepsis,seps,cat1_Cirrhosis,trauma,race_white,cat1_Coma,pot1,hema,cat1_COPD,t3d30,wtkilo1,race_other,U) = P(death|swang1,immunhx,amihx,liverhx,hema1,renal,cat1_Lung Cancer,meta,ortho,paco21,pafi1,psychhx,resp,malighx,wblc1,sex_Male,temp1,crea1,gastr,chrpulhx,ca,gibledhx,age,meanbp1,scoma1,neuro,cat1_Colon Cancer,alb1,resp1,bili1,hrt1,aps1,dementhx,transhx,cat1_CHF,cat1_MOSF w/Malignancy,card,chfhx,ph1,surv2md1,sod1,cardiohx,renalhx,cat1_MOSF w/Sepsis,seps,cat1_Cirrhosis,trauma,race_white,cat1_Coma,pot1,hema,cat1_COPD,t3d30,wtkilo1,race_other)
    ### Estimand : 2
    Estimand name: iv
    No such variable found!
    


Since now that we have the required expression, we can use that to estimate the average treatment effect (ATE).


```python
estimate = model.estimate_effect(identified_estimand,
        method_name="backdoor.linear_regression")
print('\n')
print("Average Treatment Effect estimate is " + str(estimate.value))
```

    /usr/local/lib/python3.6/dist-packages/statsmodels/tools/_testing.py:19: FutureWarning: pandas.util.testing is deprecated. Use the functions in the public API at pandas.testing instead.
      import pandas.util.testing as tm
    INFO:dowhy.causal_estimator:INFO: Using Linear Regression Estimator
    INFO:dowhy.causal_estimator:b: death~swang1+immunhx+amihx+liverhx+hema1+renal+cat1_Lung Cancer+meta+ortho+paco21+pafi1+psychhx+resp+malighx+wblc1+sex_Male+temp1+crea1+gastr+chrpulhx+ca+gibledhx+age+meanbp1+scoma1+neuro+cat1_Colon Cancer+alb1+resp1+bili1+hrt1+aps1+dementhx+transhx+cat1_CHF+cat1_MOSF w/Malignancy+card+chfhx+ph1+surv2md1+sod1+cardiohx+renalhx+cat1_MOSF w/Sepsis+seps+cat1_Cirrhosis+trauma+race_white+cat1_Coma+pot1+hema+cat1_COPD+t3d30+wtkilo1+race_other


    
    
    Average Treatment Effect estimate is 0.034360463673062114


Thus the value of the estimate for Average Treatment Effect is 0.034. This signifies that use of right heart catheterization method(treatment) increases the probabilty of death by 0.03436046367306156. And hence RHC cannot be attributed to the increase in the number of deaths due to heart surgeries.

### Sensitivity Analysis

Since that we have the estimate for our avergae treatment effect, we can proceed with our sensitivity analysis to identify the effect of hidden bias on the estimate by the model.


```python
res_random=model.refute_estimate(identified_estimand, estimate, method_name="random_common_cause")
print(res_random)
```

    INFO:dowhy.causal_estimator:INFO: Using Linear Regression Estimator
    INFO:dowhy.causal_estimator:b: death~swang1+immunhx+amihx+liverhx+hema1+renal+cat1_Lung Cancer+meta+ortho+paco21+pafi1+psychhx+resp+malighx+wblc1+sex_Male+temp1+crea1+gastr+chrpulhx+ca+gibledhx+age+meanbp1+scoma1+neuro+cat1_Colon Cancer+alb1+resp1+bili1+hrt1+aps1+dementhx+transhx+cat1_CHF+cat1_MOSF w/Malignancy+card+chfhx+ph1+surv2md1+sod1+cardiohx+renalhx+cat1_MOSF w/Sepsis+seps+cat1_Cirrhosis+trauma+race_white+cat1_Coma+pot1+hema+cat1_COPD+t3d30+wtkilo1+race_other+w_random


    Refute: Add a Random Common Cause
    Estimated effect:(0.034360463673062114,)
    New effect:(0.034354593552024304,)
    


The value of the new effect does not deviate significantly from the estimated value. Hence we can conclude that our model is not influenced by the hidden biased anad that the confounders we have controlled are sufficient for the estimation of the effect of Right Heart Catheterization on the death of the patient.

## References 

[Coursera Course - A Crash Course in Causality: Inferring Causal Effects from Observational Data](https://www.coursera.org/learn/crash-course-in-causality)  
[DoWhy package documentation](https://microsoft.github.io/dowhy/dowhy_simple_example.html)  
[Right Heart Catheterization document by Vanderbilt](http://biostat.mc.vanderbilt.edu/wiki/pub/Main/DataSets/rhc.html)   
