# Basic Theory of Casusal Inference

## Potential outcomes and counterfactuals

Suppose we are interested in the causal effect of some treatment $A$ for some observed outcome $Y$. Remember word treatment and exposure can be used interchangeably. The simplest form causal inference is where there is only one treatment and only one outcome. There can be multiple levels of treatment and outcome.  
Here let us consider that treatment and outcome can take only binary values. So there are two possible values of treatment and the same goes for outcome. So one value of treatment signify that we are exposed to some element or environmental condition and the other value signify that we are not exposed to those elements and environmental consition i.e no treatment. The above argument is valid for the outcome as well, i.e. one value signify that some effect has taken place anad the other value signify that no effect has taken place. For data analysis we need treatment and outcome to be some numbers, hence treatment is denoated by 1 and no treatment is denoted by 0. Similarly, effect that was observed is denoted by 1 and no effect is denoted by 0.

### Potential outcome

Potential outcome is basically a hypothetical outcome. It is the outcome that would have been observed under eacch poosible treatment option. Potential outcome is denoted by $Y^a$ when the treatment $A$ is set to $a$. For example, suppose the treatment is exposure to influenza vaacine and outcome be the onset of flu. So $Y^1$ is the time until the individual would get the flu if that individual receive influenza vaccine. Similarly, $Y^0$ is the time until the individual would get the flu has that individual did not receive influenza vaccine. Here we say that *'a perrson would experience outcome'* signifies that the potential outcome is a hypothetical term. This is the situation before any sort of data is collected and even before any treatment was assigned.  

### Counterfactual outcome

The other term counterfactual outcome is considered after the data is collected. Counterfactual outcome is the outcome when we try to analyze what would have happened had the treatment been different. So if the treatment is $A=1$, the counterfactual outcome become $Y^0$ which is the potential outcome when my is $A=1$. Considering the case study of influenza vaccine, the question for counterfactual outcome would arise as *'Had I not gotten the vaaacine, would I have gotten sick?'*. So here the counterfactual exposure is $A=0$ and the counterfactual outcome is $Y^0$. Thus counterfactual outcome is the outcome that would be seen under some hypothetical alternative scenario. Counterfactual outcomes are typically assumed to be same as potential outcomes. They are often used interchangeably.

## Hypothetical Interventions

We need some well defined actions in order to study the causal effects. These actions are called interventions. It is necessary to study interventions because it is cleanest to think of causal effects of interventions or actions. Interventions are defined as variables that can be manipulated either hypothetically or practiccally. It is difficult to think about causal effects for variables that you can't manipulate. If variables can be intervene on, or can be manipulated, doesn't matter whether we manipulate it hypothetically or practically, or take action on, we can define the causal effects of those variables. In this case there is usually only one version of treatment known as no hidden version of treatment. There can be variables for which there are multiple number of treatment which might be associated with different outcomes. So we can observe that, the more difficult it is to manipulate a variable, the more diffifcult it is to define their causal effect. We focus on casual effects of hypothetical interventions because their meaning is well defined and they are potentially actionable..  
A causal effect occurs when the potential outcomes $Y^1$ and $Y^0$ are not equal to each other. In general we would say that if an exposure or treatment $A$ had a causal effect on outcome $Y$ if and only if the potential outcome under treatment $Y^1$ differs from the potential outcome under no treatment $Y^0$. 
Now the Fundamental Problem of Causal Inference is that we can only observe one potential outcome for each person. To deal with the fundamental problem of causal inference, we can make certain assumptions to estimate what's known as a population or an average cost and effect. So what we can never know is what might be called a unit level causal effect or an individual level causal effect.

## Causal Effect

Now we are in a position to define Average Causal Effect. Consider a hypothetical population which resides on two hypothetical worlds simultaneously, named World 1 and World 2. The population on World 1 is exposed to some treatment while at the same time the population on World 2 is kept unexposed. Now obtain mean potential outcomes for both the worlds. The difference between them is known as Average Causal Effect. Mathematically denoted by $E(Y^1-Y^0)$. In other words, causal effect helps us to compare what would have happened if the same population is treated versus if the same population is not-treated. Cases where the potential outcome is binary, average causal effect is known as causal risk difference.  
In reality this cannot be measured. We will always have different subpopulations which will be exposed to different treatments. The expected value or mean of potential outcome for such subpopulation is denated by $E(Y|A=a)$. The subpopulations might be characteristically different than each other. This makes observed data to be different than potential outcome. And in general,
$$
E(Y^1-Y^0) \neq E(Y|A=1) - E(Y|A=0)
$$
There are different forms of causal effects for different forms of problems. Some of the commonly used ones are:  
- $E(Y^1/Y^0)$ : causal relative risk  
- $E(Y^1-Y^0 | A=1)$ : causal effect of treatment on the treated. This is related to how well the treatment works on the treated  
- $E(Y^1-Y^0 | V=v)$ : average causal effect in the subpopulation with covariate $V=v$  

### Causal Assumptions

There are four assumptions that we need to make to connect observed data with the potential outcomes. These assumptions are untestable and are known as causal assumtions. The assumptions are:  
1. Stable Unit Treatment Value Assumption (SUTVA)  
2. Consistency  
3. Ignorability  
4. Positivity  

#### SUTVA

This assumption itself composed of two assumptions.  
1. No interference - This tells that units under consideration do not interfere with each other. Here interference of treatment of one with the outcome of other unit is considered. The other terms which are used for interference are *'spillover'* and *'contagion'*.  
2. There exist only one version of treatment. And treatment exists in such a way that we can hypothetically intervene on it.  
The advantage of SUTVA is that we can write potential outcomes for the *i*th person in term of only that person's treatment.  

####  Consistency

The potential outcome under treatment $A=a$, $Y^a$, is equal to the observed outcome if the actual treatment received is $A=a$. Mathematically, $Y=Y^a \iff A=a, \forall a$.

#### Ignorability

Given pre-treatment covariates $X$, treatment assignment is independent from the potential outcomes. This assumption is also known as no unmeasured confounders assumption. Mathematically, it is represented as $Y^0, Y^1 \perp\!\!\!\perp A | X$.

#### Positivity

The positivity assumption refers to the idea that everybody had some chance of getting either treatment, and that's sort of conditional on covariates $X$. It essentially states that, for every set of values of $X$, treatment assignment was not deterministic. Mathematically, $P(A=a | X=x)>0, \forall a,x$. We require this assumption because we need to have data where we can learn about what would have happen under either treatment scenario. If, for some values of $X$, treatment was deterministic, then we would have no observed values of $Y$ for one of the treatment groups for those values of $X$. Variability in treatment assignment is important for identification of causal effect. If for certain level of $X$, the treatment is deterministic for some population then it's better to exclude that subpopulation from the study.

## Confounding

Now there are certain set of variables which can affect the treatment and the outcome, therby violating the ignorability assumption. These set of variables are known as confounders. We want treatment assignment to be completely randomized and hence it is necessary for us to identify the confounders. If we could identify variables $X$ that will hold ignorability assumption, then this set of variables is sufficient to control for confounding. It is important to note that confounders affect both treatment and outcome. We can not call a variable which affects only treatment or only outcome as confounder. There are two criteria for identifying set of variables that are sufficient to control for confounding:  
1. Backdoor path criterion  
2. Distinctive cause criterion  
But even these criteria cannot help in real world datasets which have large number of variables that act as confounders and thus we require some other mechanisms to control for confounding.

## Randomized Trial

In randomized trial, the treatment assignment is randomized so that the ignorability assumption holds. The distribution of covariates or pre-treatment variables $X$ that affect the outcome $Y$ is same for both the treatment groups due to randomization. This similariy in the distribution is called covariate balance. Thus, if the outcome differ in the both the treatment groups, it won't be because of the differences in the distribution of $X$. The pre-treatment variables are dealt with at the design phase. We don't always go for randomized trials because of the number of reasons including but not limited to:  
- they are expensive to conduct than observational studies  
- needs to be reviewed for ethics  
- lot of time required to gather data  
- need people to enroll in the study  
- follow up people over the period of time  
- follow lots of protocols, etc.  
Sometimes randomizing treatment or exposure might be unethical. Since the covariates are balanced here and all the causal assumptions hold together, it is easier to estimate the causal effect.

## Observational Study

In observational study, the treatment assignmeent is not under the our control We just keep a record of different pre-treatment covariates over a period of time. Observational study overcome many of the problems that exists for conducting randomized trial. A protocols that are required to followed as also much less as compared to the randomized trials. The major difference between a randomized trial and observational study is that we intervene on variables in the randomized trial while it is not the case with observational study where we just record the variables.  
The distribution of covariates $X$ will differ between the treatment groups in the observational study. And so in order to achieve covariates balance in the observational study, we need to perform matching. With matching, we attempt to make observational study more like randomized trial. Matching helps to control the confounding without using the outcome. The other advantage of matching over other methods used for confounding  is that it can reveal the lacck of overlap in the covariate distribution. If the overlap is absent between the covariate distribution, then the positivity assumption is violated.

