# References

Pearl, Judea. ["Causal inference in statistics: An overview"](http://ftp.cs.ucla.edu/pub/stat_ser/r350.pdf)  
  
Jason A. Roy. ["Coursera: Crash Course in Causality"](https://www.coursera.org/learn/crash-course-in-causality/lecture/x4UMR/confusion-over-causality)  
  
Will Kenton. ["Spurious Correlation"](https://www.investopedia.com/terms/s/spurious_correlation.asp)  
  
