# Matching

In the basic theory, it must have been clear that the distribution of covariates sufficient to control for confounding may differ in case of observational study and hence it is necessary to match units in the treatment group with that in the control group or non-treatment group. This process of matching covariates of the treated subjects with the control subjects is called matching. Usually in matching, if we begin with the treated group and then try to find controls that are good matches for them, it is essentially inference about the treated population and the estimate related to such scenario is called as the causal effect of treatment on the treated.  
The simplest form of matching where we just have one variable which is sufficient to control for confounding and then we try to find units in control group similar to that in the treated group such that the covariates values should match exactly or should be close enough to each other. But this form of matching can't be practically used if the number of covariates to control for confounding is more. To deal with data having more number of covariates, distance based matching is used.  

<hr>

## Matching Algorithms

### Greedy matching

In this type of matching, the treated and the control group are randomly ordered after finding the distances. Then starting from the top, entries with the least distnace is picked up and are marked as paired which are identifiable with match id and removed from the groups. The process repeats till all the entries in the treated group are considered. We can either achieve one-to-one matching and many-to-one matching. In other words, we can either associate one treatment unit with one control unit or we can associate multiple control units with one treatment unit. Choosing which method to go with depends upon the bias-variance tradeoffs. With one-to-one matching we have less bias but more variance and with many-to-one matching we have more bias but less variance. 

#### Advantages of greedy matching

- It is intuitive and hence easy to explain  
- Computaionally fast as it involves series of simple algorithms  

#### Disadvantages of greedy matching

- Not invariant to initial order of list  
- It is not globally optimal and as a result of this, it could lead to some bad matches

### Optimal Matching

In optimal matching, the treatment units are matched with the control units in such a manner that the sum of the difference between the distances is kept minimum. This is unlike the case with that of greedy matching which is not globally optimal. Here we may not select the best match in order to keep the sum of difference minimum.

#### Advantages of optimal matching

- It achieves global optimum

#### Disadvantages of optimal matching

- It is computationally demading

<hr>

## Distance Metrics

### Mahalanobis distance and Robust Mahalanobis distance

Mahalanobis distance is one of the distance used for the distance based matching. The Mahalanobis distance between covariates for subjects i and j is given by:
$$
D(X_i, X_j) = \sqrt{(X_i-X_j)^TS^{-1}(X_i-X_j)}
$$
Here,  
$X_i,X_j$ are the vectors of covariates for subjects $i$ and $j$ respectively.  
$S$ is the covariance of $X$.  
Thus Mahalanobis diatnce can be thought of as the sqaure root of the sum of squared distances between each covariiate scaled by the covariance matrix. The covariance matrix, $S$, is the scaling of covariates by the variance of that covariate. This is required so as to account for unit change in each variable remain equivalent to each other. One of that problem that is widely encountered with this method for matching is that the outliers can create large distances between objects, eben if the covariates are otherwise similar. In order to overcome this limitation Robust Mahalanobis distance is used which incorporates two following changes:  
1. Change the values into their corresponding ranks.  
2. The diagonal of covariance matrix should be constant as ranks are on the same scale now. 

### Propensity score

Propensity score is the probability of receiving treatment, rather than control, given covariates $X$. It is denoted by $\pi_i$. Mathematically, $\pi_i = P(A=1 | X_i)$. The propensity score is defined for individual unit which is denoted by the subscript $i$. Propensity score is not associated with just one set of covariates rather there can be multiple sets of covariates that can lead to same propensity score.  
Propensity score is also known as balancing score. For this to understand, consider a scenario where two subjects have same value of propensity score but different values of covariates. This means that even though they have different characteristics, they are equally likely to be treated. This means that both subjects' covariates is just as likely to be found in the treatment group. If we restrict to a subpopulation of subjects who have the same value of propensity score, then stratify on actual treatment received, the distribution of covariates in the two treatment groups should be same i.e. the two treatment groups are said to be balanced. More formally,
$$
P(X=x | \pi(X)=p, A=1) = P(X=x | \pi(X)=p, A=0)
$$ 
The implication of this is that matching on propensity score should achieve balance.  
In a randomized trial, the propensity score is generally. But this is not the case with observational data i.e. propensity score in unknown. It is important to note that propensity score involves observed data and hence we can estimate it. Most of the time when people talk about of propensity score, they are referring to the estimated propensity score. So inorder to estimate propensity any model which is capable of predicting binary outcome can be used. Most used one is Logistic Regression.

#### Propensity score matching

We saw that prropensity score is a balancing score, so matching on a propensity score should achieve balance. So it should achieve balance on the covariate distribution between treated and controlled subjects eventhough we did not directly match on the covariates. Also each unit is associated with only one propensity score, i.e. there will be only one number associated with each individual which lie between zero and one. This simplies the problem as we need to match only on one variable as opposed to the whole set of variables.  
Before matching it is necessary for us look for overlap in the propensity distribution and this is necessary to verify the positivity assumption. We essentially verify whether all the subjects had atleast some positive probability of receiving either treatment. This task is achieved with the help of histogram plot. It is necessary for us to eliminate units from analyses which are guaranteed to get either control condition or the treated condition. This process of eliminating units with extreme scores when there is lack of overlap in propensity score distribution is called trimming of tails. Trimming also avoids extrapolation.  
To match, we can either use greedy matching or optimal matching. The distance mease here would be the distance based on propensity scores rather than the distance based on covariates. The other thing worth mentioning is that instead of using untransformed propensity score, people prefer to use logit transformation which will basically stretch out the entire distribution and helps to make the matching easier. We can use caliper here also inorder to avoid bad matches. The caliper that is most used is 0.2 times the standard deviation of logit of propensity score.  

<hr>

## Analyzing Data

The methods that are used for analyzing data are:  
1. Randomized Tests / Permutation Tests/ Exact tests  
2. Conditional Logistic Regression  
3. Stratified Cox Model  
4. Generalized estmating equations (GEE)  
It is also necessary to analyse the sensitivity of model to the hidden which can be achieved by sensitivity analyses.  

<hr>

## Other concepts

### Caliper

We might want to exclude subjects which does not have good matches. This is achieved using caliper. Caliper is the maximum distance which can be accepted for matching treatment units with control units. So if the distance is not less than this caliper then we can discard that treatment unit from the analysis. Matching with caliper helps the positivity assumption to be more plausible as caliper helps to identify individuals within treatment which could not form a good match with any of the individual in the control group, further suggesting that this treated subject really didn't have much of a chance of being a control subject. One drawback of using this metric is that the population becomes difficult to define.
