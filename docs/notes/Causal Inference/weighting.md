# Weighting

## Inverse Probability of Treatment Weighting (IPTW)

### Introduction

Though different Strategies of weighting are available, inverse probability of treatment weighting is one of the most reknowned ones. It may be seen in the propensity score one-to-one matching that in the presence of imbalance class, we may discard a lot of data from one group. And hence in such cases in order to use almost all the data it is necessary to weight the units so that both the groups i.e. treated and non-treated contribute equally to the analysis. One of the ways to weight the data is by weighting with the inverse of propensity scores. We weight treated group with the weights obtained by inversing the propensity score of treated group while for non-treated, we us inverse of the propensity score of non-treated group. In general, we weight by the propensity score of whichever group that unit belongs to. Thus weighting, up-weights the units with less number and down-weights the units with more number. This makes sure that contribution of each group is equal to the estimation of causal effect.  
To build further intuition IPTW, think of a survey where we have imbalance classes and then we oversample the class which is smaller in size. And so to estimate a mean of the whole population, and not just of a subpopulation, data needs to be weighted so as to account for the oversampling. This kind of approach is known as Horowitz-Thompson estimator. The situation actually relates to the observational study where we have a lot of confounders and then we typically going to have oversampling of either the treated group or the control group at various values of covariates. Here, weighting can be used to get back to the original population. In order words, we are creating a pseudo-population with no confounding just to mimic a randomized trial. In pseudo-population, everybody is equally like to be treated and it wouldn't depend on the covariates.  

### A simple causal effect

Under the assumption of exchangeability (ignorability) and positivity, we can estimate $E(Y^1)$ as 
$$
\frac{\sum_{i=1}^{n} I(A_i=1)\frac{Y_i}{\pi_i}}{\sum_{i=1}^{n} \frac{I(A_i=1)}{\pi_i}}
$$  
where, $\pi_i = P(A=1|X_i)$ is the propensity score and $I$ is the indicator function which acts as a on-off switch which will help us to consider only treated units in the above formula.

### Marginal Structural Model

Marginal Structural Model is for estimating the mean of potential outcome or population average causal effect. The word 'marginal' itself signifies averaging over whole population. The structural part of the phrase has to do with the fact that we are modelling potential outcomes as opposed to observed outcomes.  

#### Linear MSM

Let us start with linear MSM first, which is given by,  
$$
E(Y^a)=\psi_o +\psi_1a, \ni a=0,1
$$  
Here, $E(Y^0) = \psi_0$,  
$E(Y^1) = \psi_0+\psi_1$  
Thus $psi_1$ is the average causal effect i.e. $E(Y^1)-E(Y^0)$
This model is typically used for continuous outcome. Remember here we are considering potential outcome instead of observed outcome.

#### Logistic MSM for binary outcome

Logistic MSM for binary outcome is given by:
$$
logit[E(Y^a)] = \psi_0 +\psi_1a, \ni a=0,1
$$  
Notice that here we are talking about potential outcome $Y^a$. The causal odds ratio is the $exp(\psi_0)$ and is given by,
$$
\frac{\frac{P(Y^1=1)}{1-P(Y^1=1)}}{\frac{P(Y^0=1)}{1-P(Y^0=1)}}
$$
Numerator represents odds that $Y^1=1$ i.e. odds that everybody in the population is treated and denominator represents odds that $Y^0=1$ i.e. odds that everybody in the population is kept un-treated.

#### MSM with Effect Modification

MSMs can also include effect modifiers and this is also known as heterogeneity of treatment effect. In order words, treatment might vary across subpopulations and we might be interested in that. Suppose $V$ is some variable that modifies the effect of $A$. A linear MSM with effect modification:
$$
E(Y^a|V) = \psi_0 +\psi_1a+\psi_3V+\psi_4aV, \ni a=0,1
$$
Thus, 
$$
E(Y^1|V)-E(Y^0|V) = \psi_1+\psi_4V
$$

#### General MSM

The general MSM is given by,
$$
g[E(Y^a|V)] = h(a,V;\psi)
$$  
where $g()$ is the link function and $h()$ is a function specifying parametric form of $a$ and $V$ (typically additive, linear)

So in all the above models if we could estimate the values of $\psi$s, then we can estimate the average causal effect. But that is not at all straightforward.

#### Estimation in MSM

Consider estimation of parameters from a generalized linear regression model:
$$
E(Y_i|X_i) = \mu_i = g^{-1}(X^T_i\beta)
$$  
Estimation of parameter $\beta$ involves solving,

$$
\sum_{i=1}^n \frac{\partial \mu^T_i}{\partial \beta}V^{-1}_i[Y_i-\mu_i(\beta)] = 0
$$

The marginal structural model $E(Y^a_i) = g^{-1}(\psi_0 + \psi_1a)$ is not equivalent to the regression model $E(Y_i|A_i) = g^{-1}(\psi_0 + \psi_1A_i)$ because of confounding. In the MSM we are strictly restricting ourselves to the potential outcome instead of conditioning over $A$. This is applicable in case of observed data. But in case of randomized trial both the above equations will be equal and can be used for the estimation of $\psi$.  
So in order to use the estimating equation for generalized linear model, we can obtain psuedo-population from IPTW which is free from confounding. Thus observed data of pseudo population can be used to solve for $\psi$, 

$$
\sum_{i=1}^n \frac{\partial \mu^T_i}{\partial \beta}V^{-1}_iW_i[Y_i-\mu_i(\psi)] = 0
$$

Here $W_i = \frac{1}{A_iP(A=1|X_i)+(1-A_i)P(A=0|X_i)}$  

##### Steps involve in estimation in MSM

1. Estimate propensity score
2. Create weights $W_i$
3. Specify MSM of interest
4. Use software to fit a weighted generalized linear model
5. Use asymptotic variance estimator or bootstrapping to account for the fact that pseudo-population might be larger than sample size


### Assessing Balance

It is necessary to check if weighting is working in our favor and hence covariate balance needs to be checked. Covariate balance can be checked on the weighted sample using standardized difference and to observe those differences we can either use table 1 or plot. A standardized difference is the difference in means between groups, divided by the pooled standard deviation. Mathematically, 

$$
smd = \frac{\bar{X}_{treatment} - \bar{X}_{control}}{\sqrt{\frac{s^2_{treatment}+s^2_{control}}{2}}}
$$

Usually, absolute value standardized mean difference is considered as we are only interested in the magnitude. Standardized mean difference is calculated for every covariate before weighting and after weighting and then compared with each other. After weighting, weighted means and weighted variances are considered for the calculation of standardized mean difference. SMD value should be less than 0.1 but if it exists between 0.1 to 0.2, then that value od SMD is considered to be tolerable. But anything greater than 0.2 is not acceptable and needs to be treated in a different manner. One thing that can be done is refining the propensity score model. Or else we can remove that variable from the analysis.

### Distribution of weights and Remedies for large weights

One of the issue that we might encounter using IPTW is existence of large weights, and that's because, large weights can lead to large standard errors. It is as if one unit is greatly affecting the analyses. One way to estimate standard error is by bootstrapping. Steps involved in bootstrapping are:  
1. Randomly sample, with replacement, from the original sample  
2. Estimate parameters  
3. Repeat steps 1 and 2 many times  
4. The standard deviation of the bootstrap estimates is an estimate of the standard error  
So now consider if a person is having large weight and we are performing bootstrapping, then that one person is going to greatly increase the variability of the estimator and this is in a sense, a noise in the data. This noise will affect the average causal effect by nearly violating the positivity assumption. This we can be infer from the situation where we have a lerge weight and that means the propensity score for that person, for whichever treatment group that person was representing, was near to zero and thus the probability of that person of getting either treatment was close to zero. This is close to the violation of positivity assumption.  
One of the easiest way to find such abnormal weights in the data is by plotting the distribution of weights.  

The first step that we need to address this problem of large weights is knowing why the weights are large. Identify the units having large weights and find out what is unusual about them, is there a problem with the data or is there a problem with the propensity score model. It may or may not possible for us to act upon the problems even if we identify them. We can try rectifying the propensity score model if there isn't any problem with the data. So one of the things which is usually done in the cases of larges weights is that we can trim the tails based on the propensity score i.e. getting rid of the units with extreme values of the propensity scores. This trimming of tail make our positivity assumption more plausible. A common strategy used in the trimming of tails is:  
- Remove treated subjects whose propensity scores are above the 98th percentile from the distribution among controls.  
- Remove control subjects whose propensity scores are below the 2nd percentile from the distribution of treated subjects.  
Since we are trauncating the population, we are introducing some bias. But keeping those units with extreme values with introduce huge variance. That's the bias - variance tradeoff that we need to deal with. It has been shown and studied that truncating extremely large weights can result in estimators with lower mean squared error. Mean squared error is the creterion that is used to judge the bias-variance tradeoff.

## Augmented Inverse probability of treatment (Doubly Robust Estimators)

One of the way to estimate the mean of the potential outcome of the treated $E(Y^1)$ is by using IPTW:

$$
\frac{1}{n} \sum_{i=1}^n \frac{A_iY_i}{\pi_i(X_i)}
$$

If the propensity score is correctly specified, this estimator is unbiased. Here note that we are only focusing on the treated units. We can do the same for the other group as well. This is can be thought of as the mean of potential outcome in the pseudo population which is free from confounding.

Alternative to estimator which uses IPTW , we could estimate the causal effect of the treated $E(Y^1)$ by specifying an outcome outcome model $m_1(X) = E(Y|A=1,X)$ and then averaging over the distribution of $X$:

$$
\frac{1}{n} \sum_{i=1}^n[A_iY_i + (1-A_i)m_1(X_i)]
$$

This euation essentially has two components. The first component i.e.$A_iY_i$ tells us that for the treated subjects use observed value of $Y$ and the other component $(1-A_i)m_1(X_i)$ tells us that for the other subjects, use predicted value of Y given their covariates if they had been treated. This is the valid estimate of the mean potential outcome as long as we have unconfoundedness given the covariates. If the outcome model is correctly specified, then this estimator is unbiased.

Using the above two models, we can build a doubly robust estimator. A doubly robust estimator is an estimator that is unbiased if either the propensity score model or the outcome regression model are correctly specified. Example:

$$
\frac{1}{n} \sum_{i=1}^n \Bigg\{ \frac{A_iY_i}{\pi_i(X_i)} - \frac{A_i-\pi_i(X_i)}{\pi_i(X_i)} m_1(X_i) \Bigg\}
$$

